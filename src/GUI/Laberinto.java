
package GUI;

public class Laberinto {
    
    private short[][]laberinto;
    private long time;
    int veces = 0;
    
    public Laberinto(String URL){
        //this.time=0;
        this.laberinto=ArchivoLeerUrl.getLaberinto(URL);
    }

    protected short[][] getLaberinto() {
        return laberinto;
    }
    
    public String getCamino(int filaTeseo, int colTeseo,int filaMinTauro, int colMinTauro, int filaSalida, int colSalida){
        try{       
            String [] CM=new String[]{""};
            String [] CS=new String[]{""};
            this.encontrarObj(true, CM, filaTeseo, colTeseo, filaTeseo, colTeseo, filaMinTauro, colMinTauro);
            if(CM[0].isEmpty()){
                return "Teseo no pudo encontrar al minotauro.";
            }
            this.encontrarObj(false, CS, filaMinTauro, colMinTauro, filaMinTauro, colMinTauro, filaSalida, colSalida);
            if(CS[0].isEmpty()){
                return "Teseo encontró y mató el minotauro pero no pudo encontrar una salida.";
            }
            this.pintarCamino(CM[0],CS[0]);
            System.out.println("veces : "+veces);
            return CM[0]+";"+CS[0];
        }catch(Exception e){
            return "Ha habio un error en la carga de la matriz o las coordeandas usadas se salen de la misma.";
        }
    }
    
    
    
    private void pintarCamino(String cam1, String cam2){
        String [] temp1 = cam1.split(":");
        String [] temp2 = cam2.split(":");
        for(int i=1;i<temp1.length-1;i++){
            String [] YX = temp1[i].split(";");
            this.laberinto[Integer.valueOf(YX[0])][Integer.valueOf(YX[1])]=1;
        }
        for(int i=0;i<temp2.length-1;i++){
            String [] YX = temp2[i].split(";");
            this.laberinto[Integer.valueOf(YX[0])][Integer.valueOf(YX[1])]=1;
        }
    }
    
    private int encontrarObj(boolean tipo, String[] salida, int FI, int CI, int filaTeseo, int colTeseo,int filaObj, int colObj){
        veces++;
        this.dormir();
        if((filaTeseo==filaObj)&&(colTeseo==colObj)){
            salida[0]=filaTeseo+";"+colTeseo;
            if(tipo){
                this.laberinto[filaTeseo][colTeseo]=-100;
            }else{
                this.laberinto[filaTeseo][colTeseo]=-1000;
            }
            return 1;
        }
        int R=0;
        int L=0;
        int D=0;
        int U=0;
        String[]SR=new String[]{""};
        String[]SL=new String[]{""};
        String[]SD=new String[]{""};
        String[]SU=new String[]{""};
        if(this.laberinto[filaTeseo][colTeseo]==0){
            this.laberinto[filaTeseo][colTeseo]=1;
        }
        if(this.validarR(filaTeseo, colTeseo, tipo)){
            R+=this.encontrarObj(tipo, SR, FI, CI, filaTeseo, colTeseo+1, filaObj, colObj);
        }
        if(this.validarL(filaTeseo, colTeseo, tipo)){
            L+=this.encontrarObj(tipo, SL, FI, CI, filaTeseo, colTeseo-1, filaObj, colObj);
        }
        if(this.validarD(filaTeseo, colTeseo, tipo)){
            D+=this.encontrarObj(tipo, SD, FI, CI, filaTeseo+1, colTeseo, filaObj, colObj);
        }
        if(this.validarU(filaTeseo, colTeseo, tipo)){
            U+=this.encontrarObj(tipo, SU, FI, CI, filaTeseo-1, colTeseo, filaObj, colObj);
        }
        if(this.laberinto[filaTeseo][colTeseo]==1){
            this.laberinto[filaTeseo][colTeseo]=0;
        }
        if(R>0&&(R<=L||L==0)&&(R<=U||U==0)&&(R<=D||D==0)){
            if(!tipo && FI==filaTeseo && CI==colTeseo){
                salida[0]=SR[0];
                return R;
            }
            salida[0]=filaTeseo+";"+colTeseo+":"+SR[0];
            return R+1;
        }
        if(L>0&&(L<=R||R==0)&&(L<=U||U==0)&&(R<=D||D==0)){
            if(!tipo && FI==filaTeseo && CI==colTeseo){
                salida[0]=SL[0];
                return L;
            }
            salida[0]=filaTeseo+";"+colTeseo+":"+SL[0];
            return L+1;
        }
        if(D>0&&(D<=R||R==0)&&(D<=L||L==0)&&(D<=U||U==0)){
            if(!tipo && FI==filaTeseo && CI==colTeseo){
                salida[0]=SD[0];
                return D;
            }
            salida[0]=filaTeseo+";"+colTeseo+":"+SD[0];
            return D+1;
        }
        if(U>0&&(U<=R||R==0)&&(U<=L||L==0)&&(U<=D||D==0)){
            if(!tipo && FI==filaTeseo && CI==colTeseo){
                salida[0]=SU[0];
                return U;
            }
            salida[0]=filaTeseo+";"+colTeseo+":"+SU[0];
            return U+1;
        }
        this.dormir();
        return 0;
    }
   
    private void dormir(){
        try{
            Thread.sleep(this.time);
        }catch (Exception e){}
    }
    
    private boolean validarR(int filaTeseo, int colTeseo, boolean tipo){
        if(tipo){
            return((colTeseo+1<this.laberinto[0].length)&&(this.esCamino(filaTeseo, colTeseo+1)||this.MR(filaTeseo, colTeseo)));
        }
        return((colTeseo+1<this.laberinto[0].length)&&(this.esCamino(filaTeseo, colTeseo+1)||this.SR(filaTeseo, colTeseo)));
    }
    
    private boolean validarL(int filaTeseo, int colTeseo, boolean tipo){
        if(tipo){
            return((colTeseo-1>=0)&&(this.esCamino(filaTeseo, colTeseo-1)||this.ML(filaTeseo, colTeseo)));
        }
        return((colTeseo-1>=0)&&(this.esCamino(filaTeseo, colTeseo-1)||this.SL(filaTeseo, colTeseo)));
    }
    
    private boolean validarD(int filaTeseo, int colTeseo, boolean tipo){
        if(tipo){
            return((filaTeseo+1<this.laberinto.length)&&(this.esCamino(filaTeseo+1, colTeseo)||this.MD(filaTeseo, colTeseo)));
        }
        return((filaTeseo+1<this.laberinto.length)&&(this.esCamino(filaTeseo+1, colTeseo)||this.SD(filaTeseo, colTeseo)));
    }
    
    private boolean validarU(int filaTeseo, int colTeseo, boolean tipo){
        if(tipo){
            return((filaTeseo-1>=0)&&(this.esCamino(filaTeseo-1, colTeseo)||this.MU(filaTeseo, colTeseo)));
        }
        return((filaTeseo-1>=0)&&(this.esCamino(filaTeseo-1, colTeseo)||this.SU(filaTeseo, colTeseo)));
    }
    
    private boolean esCamino(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo][colTeseo]==0;
    }
    
    private boolean MR(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo][colTeseo+1]==100||this.laberinto[filaTeseo][colTeseo+1]==100;
    }
    
    private boolean ML(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo][colTeseo-1]==100||this.laberinto[filaTeseo][colTeseo-1]==-100;
    }
    
    private boolean MD(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo+1][colTeseo]==100||this.laberinto[filaTeseo+1][colTeseo]==-100;
    }
    
    private boolean MU(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo-1][colTeseo]==100||this.laberinto[filaTeseo-1][colTeseo]==-100;
    }
    
    private boolean SR(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo][colTeseo+1]==1000||this.laberinto[filaTeseo][colTeseo+1]==-1000;
    }
    
    private boolean SL(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo][colTeseo-1]==1000||this.laberinto[filaTeseo][colTeseo-1]==-1000;
    }
    
    private boolean SD(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo+1][colTeseo]==1000||this.laberinto[filaTeseo+1][colTeseo]==-1000;
    }
    
    private boolean SU(int filaTeseo, int colTeseo){
        return this.laberinto[filaTeseo-1][colTeseo]==1000||this.laberinto[filaTeseo-1][colTeseo]==-1000;
    }
   

    
}
